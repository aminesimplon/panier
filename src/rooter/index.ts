import HomeView from "@/views/HomeView.vue";
import NotFound from "@/views/NotFound.vue";
import { createRouter, createWebHistory } from "vue-router";

export const router  = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/', component: HomeView},
        { path: '/:pathMatch(.*)*', component: NotFound }
    ]
}); 


